@app.route('/files/<path:path>')
def get_file(path):
    """Download a file."""
    global current_mail
    if current_mail!="0":
    	UPLOAD_DIRECTORY="/Users/aditya/database/"+current_mail
    	return send_from_directory(UPLOAD_DIRECTORY, path, as_attachment=True)
    else:
    	return render_template('error.html')