import os
from flask import Flask, request, redirect, url_for, send_from_directory, render_template , flash
from werkzeug.utils import secure_filename
import sqlite3


UPLOAD_FOLDER = '/Users/aditya/database/' + email;
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)     

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif','cpp','c'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

email="sdss"
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            fpath=os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(fpath)
            fp=(fpath,fpath)
            conn=sqlite3.connect('users.db')
            c=conn.cursor()
            c.execute('insert into paths values(?,?)',fp)
            return redirect(url_for('uploaded_file',filename=filename))
    return render_template('upload.html')

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],filename)

if __name__ == '__main__':
   app.run(debug=True)
