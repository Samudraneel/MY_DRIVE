import os
from flask import Flask , render_template,request


app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
@app.route('/')
def index():
	return render_template('upload.html')

@app.route("/upload", methods=["POST"])
def upload():
	target = os.path.join(APP_ROOT,'images/')
	print "target "

	if not os.path.isdir(target):
		os.mkdir(target)

	for f in request.files.getlist("file"):
		print(f)
		filename= f.filename
		destination="/".join([target,filename])
		print (destination)
		f.save(destination)

	return render_template('comp.html')

if __name__ == '__main__':
	app.run(debug=True)